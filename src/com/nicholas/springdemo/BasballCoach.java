package com.nicholas.springdemo;

public class BasballCoach implements Coach {
	
	private FortuneService fortuneService;
	
	public BasballCoach ( FortuneService theFortuneService ) {
		fortuneService = theFortuneService;
	}
	
	@Override
	public String getDailyWorkout () {
		return "Spend 30 minutes on batting practice.";
	}

	@Override
	public String getDailyFortune() {
		
		return fortuneService.getFortune();
	
	}
}

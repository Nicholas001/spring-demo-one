package com.nicholas.springdemo;

public class CricketCoach implements Coach {

	private FortuneService fortuneService;
	
	private String emailAddress;
	
	private String team;
	
	
	public CricketCoach () {
		System.out.println("CricketCoach : the constructor whit no-arg was called.");
	}
	
	
	
	public String getEmailAddress() {
		return emailAddress;
	}



	public void setEmailAddress(String emailAddress) {
		System.out.println("CricketCoach : the email setter method was called.");
		this.emailAddress = emailAddress;
	}



	public String getTeam() {
		return team;
	}



	public void setTeam(String team) {
		System.out.println("CricketCoach : the team setter method was called.");
		this.team = team;
	}



	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("CricketCoach : the setter method was called.");
		this.fortuneService = fortuneService;
	}



	@Override
	public String getDailyWorkout() {
		return "Practice fast bownling for 15 minutes ";
	}

	
	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

}

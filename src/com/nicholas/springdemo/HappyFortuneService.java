package com.nicholas.springdemo;

import java.util.ArrayList;
import java.util.Random;

public class HappyFortuneService implements FortuneService {

	ArrayList fortuneList = new ArrayList();
	private Random myRandom = new Random();
	
	@Override
	public String getFortune(){
		fortuneList.add("Today is your lucky day!");
		fortuneList.add("Today is a bad day for sport!");
		fortuneList.add("Today you will win the game!");
		int index = myRandom.nextInt(fortuneList.size()); 
		String text=(String) fortuneList.get(index);
		return text;
	}

}
